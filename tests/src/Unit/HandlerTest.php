<?php

namespace Drupal\Tests\amazon_ses\Unit;

use Aws\SesV2\Exception\SesV2Exception;
use Aws\SesV2\SesV2Client;
use Drupal\Tests\amazon_ses\Traits\MockHandlerTrait;
use Drupal\Tests\amazon_ses\Traits\MockMessageBuilderTrait;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the Amazon SES handler service.
 *
 * @group amazon_ses
 */
class HandlerTest extends UnitTestCase {
  use MockHandlerTrait;
  use MockMessageBuilderTrait;
  use ProphecyTrait;

  /**
   * Tests that the handler successfully sends an email.
   *
   * @dataProvider messageData
   */
  public function testSend($message) {
    $message_id = $this->randomMachineName();

    $client = $this->prophesize(SesV2Client::class);
    $client
      ->sendEmail(Argument::type('array'))
      ->willReturn(['MessageId' => $message_id]);

    $message_builder = $this->getMessageBuilder();
    $handler = $this->getHandler($client);

    $email = $message_builder->buildMessage($message);
    $return = $handler->send($email);

    $this->assertEquals($return, $message_id);
  }

  /**
   * Tests that a failed send is handled.
   *
   * @dataProvider messageData
   */
  public function testFailedSend($message) {
    $exception = $this->prophesize(SesV2Exception::class);
    $exception
      ->getAwsErrorMessage()
      ->willReturn('Error message');

    $client = $this->prophesize(SesV2Client::class);
    $client
      ->sendEmail(Argument::type('array'))
      ->willThrow($exception->reveal());

    $message_builder = $this->getMessageBuilder();
    $handler = $this->getHandler($client);

    $email = $message_builder->buildMessage($message);
    $return = $handler->send($email);

    $this->assertFalse($return);
  }

  /**
   * Tests that the handler successfully sends an email.
   */
  public function testGetIdentities() {
    $client = $this->prophesize(SesV2Client::class);
    $client
      ->listEmailIdentities()
      ->willReturn($this->mockResult([
        'EmailIdentities' => [
          [
            'IdentityType' => 'DOMAIN',
            'IdentityName' => 'example.com',
            'SendingEnabled' => TRUE,
          ],
          [
            'IdentityType' => 'EMAIL_ADDRESS',
            'IdentityName' => 'email@example.com',
            'SendingEnabled' => TRUE,
          ],
        ],
      ]));

    $client
      ->getEmailIdentity(Argument::is([
        'EmailIdentity' => 'example.com',
      ]))
      ->willReturn($this->mockResult([
        'IdentityType' => 'DOMAIN',
        'VerifiedForSendingStatus' => TRUE,
        'DkimAttributes' => [
          'SigningEnabled' => TRUE,
        ],
      ]));

    $client
      ->getEmailIdentity(Argument::is([
        'EmailIdentity' => 'email@example.com',
      ]))
      ->willReturn($this->mockResult([
        'IdentityType' => 'EMAIL_ADDRESS',
        'VerifiedForSendingStatus' => TRUE,
        'DkimAttributes' => [
          'SigningEnabled' => FALSE,
        ],
      ]));

    $handler = $this->getHandler($client);
    $return = $handler->getIdentities();

    $this->assertEquals($return[0]['identity'], 'example.com');
    $this->assertEquals($return[0]['status'], 'Success');
    $this->assertEquals($return[0]['type'], 'Domain');
    $this->assertEquals($return[0]['dkim'], 'Enabled');

    $this->assertEquals($return[1]['identity'], 'email@example.com');
    $this->assertEquals($return[1]['status'], 'Success');
    $this->assertEquals($return[1]['type'], 'Email Address');
    $this->assertEquals($return[1]['dkim'], '');
  }

  /**
   * Tests getting sending quota data.
   */
  public function testSendQuota() {
    $client = $this->prophesize(SesV2Client::class);
    $client
      ->getAccount()
      ->willReturn($this->mockResult([
        'SendQuota' => [
          'Max24HourSend' => 50000,
          'MaxSendRate' => 15,
          'SentLast24Hours' => 1000,
        ],
      ]));

    $handler = $this->getHandler($client);
    $return = $handler->getSendQuota();

    $this->assertEquals($return['Max24HourSend'], '50,000');
    $this->assertEquals($return['MaxSendRate'], '15');
    $this->assertEquals($return['SentLast24Hours'], '1,000');
  }

  /**
   * Provides message data for a successful message.
   */
  public function messageData() {
    return [
      [
        [
          'to' => 'to@example.com',
          'from' => 'Test Tester <from@example.com>',
          'subject' => 'Amazon SES test',
          'body' => 'test message body',
          'headers' => [
            'Content-Type' => 'text/plain',
          ],
        ],
      ],
    ];
  }

}
