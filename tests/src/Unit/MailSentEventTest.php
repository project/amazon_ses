<?php

namespace Drupal\Tests\amazon_ses\Unit;

use Drupal\amazon_ses\Event\MailSentEvent;
use Drupal\Tests\amazon_ses\Traits\MockMessageBuilderTrait;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Mime\Email;

/**
 * Tests the Amazon SES handler service.
 *
 * @group amazon_ses
 */
class MailSentEventTest extends UnitTestCase {

  use MockMessageBuilderTrait;

  /**
   * Tests that the handler successfully sends an email.
   */
  public function testGetMessageId() {
    $message_id = $this->randomMachineName();
    $event = $this->createEvent($message_id);

    $this->assertEquals($message_id, $event->getMessageId());
  }

  /**
   * Tests that the handler successfully sends an email.
   *
   * @dataProvider messageData
   */
  public function testGetEmail($message) {
    $message_id = $this->randomMachineName();
    $event = $this->createEvent($message_id);

    $email = $event->getEmail();
    $from = $email->getFrom();

    $this->assertInstanceOf(Email::class, $email);
    $this->assertEquals($message['from'], $from[0]->getAddress());
  }

  /**
   * Create a MailSentEvent object.
   *
   * @param string $message_id
   *   The ID of the message.
   *
   * @return \Drupal\amazon_ses\Event\MailSentEvent
   *   The event object.
   */
  protected function createEvent($message_id) {
    $message = [
      'to' => 'to@example.com',
      'from' => 'from@example.com',
      'subject' => 'Amazon SES test',
      'body' => 'test message body',
      'headers' => [
        'Content-Type' => 'text/plain',
      ],
    ];

    $message_builder = $this->getMessageBuilder();
    $email = $message_builder->buildMessage($message);

    return new MailSentEvent($message_id, $email);
  }

  /**
   * Provides message data for a successful message.
   */
  public function messageData() {
    return [
      [
        [
          'to' => 'to@example.com',
          'from' => 'from@example.com',
          'subject' => 'Amazon SES test',
          'body' => 'test message body',
          'headers' => [
            'Content-Type' => 'text/plain',
          ],
        ],
      ],
    ];
  }

}
