<?php

namespace Drupal\Tests\amazon_ses\Traits;

use Aws\Result;
use Drupal\amazon_ses\AmazonSesHandler;
use Drupal\amazon_ses\Event\MailSentEvent;
use Drupal\aws\AwsClientFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Mock SES handler trait.
 */
trait MockHandlerTrait {

  /**
   * Mock the handler.
   *
   * @param \Aws\SesV2\SesV2Client|\Prophecy\Prophecy\ObjectProphecy $client
   *   The mocked SES client.
   *
   * @return \Drupal\amazon_ses\AmazonSesHandler
   *   The handler service.
   */
  protected function getHandler($client) {
    $client_factory = $this->prophesize(AwsClientFactoryInterface::class);
    $client_factory
      ->getClient(Argument::exact('sesv2'))
      ->willReturn($client->reveal());

    $logger = $this->prophesize(LoggerChannelInterface::class);
    $messenger = $this->prophesize(MessengerInterface::class);

    $event = $this->prophesize(MailSentEvent::class);
    $event_dispatcher = $this->prophesize(EventDispatcherInterface::class);
    $event_dispatcher
      ->dispatch(Argument::type(MailSentEvent::class), Argument::type('string'))
      ->willReturn($event->reveal());

    $config = $this->prophesize(ImmutableConfig::class);
    $config_factory = $this->prophesize(ConfigFactoryInterface::class);
    $config_factory
      ->get('amazon_ses.settings')
      ->willReturn($config->reveal());

    return new AmazonSesHandler(
      $client_factory->reveal(),
      $logger->reveal(),
      $messenger->reveal(),
      $event_dispatcher->reveal(),
      $config_factory->reveal()
    );
  }

  /**
   * Generate an AWS Result with mock data.
   *
   * @param array $data
   *   The data to populate the result.
   *
   * @return \Aws\Result
   *   The result.
   */
  protected function mockResult(array $data) {
    $date = new \DateTime('UTC');

    $data = array_merge($data, [
      '@metadata' => [
        'statusCode' => 200,
        'effectiveUri' => 'https://email.us-east-1.amazonaws.com',
        'headers' => [
          'date' => $date->format('D, d M Y H:i:s \G\M\T'),
          'content-type' => 'text/xml',
          'connection' => 'keep-alive',
        ],
        'transferStats' => [
          'http' => [
            0 => [],
          ],
        ],
      ],
    ]);

    return new Result($data);
  }

}
