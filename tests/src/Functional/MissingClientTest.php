<?php

namespace Drupal\Tests\amazon_ses\Functional;

use Drupal\Core\Url;

/**
 * Tests functionality when the AWS SES client is missing.
 *
 * @group amazon_ses
 */
class MissingClientTest extends FunctionalTestBase {

  /**
   * Tests the error message is shown when the client can't be initialized.
   */
  public function testErrorMessage() {
    $this->drupalGet(Url::fromRoute('amazon_ses.identities'));
    $this->assertSession()->pageTextContains('Unable to initialize the SES client.');
  }

}
