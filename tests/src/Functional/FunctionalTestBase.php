<?php

namespace Drupal\Tests\amazon_ses\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Base class for functional tests.
 */
abstract class FunctionalTestBase extends BrowserTestBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'amazon_ses',
  ];

  /**
   * User with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * User with no additional permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $basicUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer amazon ses',
    ]);

    $this->basicUser = $this->drupalCreateUser();

    $this->drupalLogin($this->adminUser);
  }

}
