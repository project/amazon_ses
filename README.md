# Amazon SES

This module is used to send email via Amazon SES, instead of using Drupal's
native mail system.


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Amazon Web Services](https://www.drupal.org/project/aws)

For full functionality, the following AWS permissions are required.

- ses:GetAccount
- ses:CreateEmailIdentity
- ses:DeleteEmailIdentity
- ses:GetEmailIdentity
- ses:ListEmailIdentities


## Installation

To ensure all dependencies are met, it is recommended to install this module
using composer.

`composer require drupal/amazon_ses`


## Configuration

- Configure Amazon Web Services with your AWS credentials.
- Verify your email address or domain at Administration » Configuration »
System » Amazon SES » Verified Identities
- Configure Drupal to use Amazon SES for sending email. The most common way to
do this is to use the [Mail System](https://www.drupal.org/project/mailsystem)
module.


## Maintainers

- Ben Davis - [davisben](https://drupal.org/u/davisben)
