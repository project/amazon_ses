<?php

namespace Drupal\amazon_ses\Event;

use Symfony\Component\Mime\Email;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines mail group message events.
 */
class MailSentEvent extends Event {

  const SENT = 'amazon_ses.mail_sent';

  /**
   * The ID of the sent message.
   *
   * @var string
   */
  protected $messageId;

  /**
   * The Email object representing the sent message.
   *
   * @var \Symfony\Component\Mime\Email
   */
  protected $email;

  /**
   * Constructs the MailSentEvent object.
   *
   * @param string $message_id
   *   The ID of the sent message.
   * @param \Symfony\Component\Mime\Email $email
   *   The Email object representing the sent message.
   */
  public function __construct(string $message_id, Email $email) {
    $this->messageId = $message_id;
    $this->email = $email;
  }

  /**
   * Gets the message ID.
   *
   * @return string
   *   The ID of the sent message.
   */
  public function getMessageId() {
    return $this->messageId;
  }

  /**
   * Gets the Email object.
   *
   * @return \Symfony\Component\Mime\Email
   *   The Email object representing the sent message.
   */
  public function getEmail() {
    return $this->email;
  }

}
