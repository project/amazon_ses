<?php

namespace Drupal\amazon_ses\Traits;

use Drupal\amazon_ses\AmazonSesHandlerInterface;

/**
 * Amazon SES handler trait.
 */
trait HandlerTrait {

  /**
   * The Amazon SES handler service.
   *
   * @var \Drupal\amazon_ses\AmazonSesHandlerInterface
   */
  protected $handler;

  /**
   * Set the handler object.
   *
   * @param \Drupal\amazon_ses\AmazonSesHandlerInterface $handler
   *   The handler object.
   *
   * @return $this
   */
  protected function setHandler(AmazonSesHandlerInterface $handler) {
    $this->handler = $handler;

    return $this;
  }

  /**
   * Verify that the AWS SES client has been initialized.
   *
   * @return bool
   *   TRUE if the client is initialized, FALSE if not.
   */
  protected function verifyClient() {
    $client = $this->handler->getClient();

    if (!$client) {
      $this->messenger()->addError($this->t('Unable to initialize the SES
        client. More information may be available in the log.'));
    }

    return (bool) $client;
  }

}
