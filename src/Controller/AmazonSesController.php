<?php

namespace Drupal\amazon_ses\Controller;

use Drupal\amazon_ses\Traits\HandlerTrait;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Amazon SES routes.
 */
class AmazonSesController extends ControllerBase {
  use HandlerTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->setHandler($container->get('amazon_ses.handler'));

    return $instance;
  }

  /**
   * Outputs a page of statistics.
   *
   * @return array
   *   A render array to build the page.
   */
  public function statistics() {
    $statistics = [];

    if (!$this->verifyClient()) {
      return $statistics;
    }

    $result = $this->handler->getSendQuota();

    if (!empty($result)) {
      $statistics = [
        'quota' => [
          '#type' => 'details',
          '#title' => $this->t('Daily sending limits'),
          '#open' => TRUE,
          'sending_quota' => [
            '#markup' => $this->t('<strong>Quota:</strong> @max_send', [
              '@max_send' => $result['Max24HourSend'],
            ]) . '<br />',
          ],
          'sent_mail' => [
            '#markup' => $this->t('<strong>Sent:</strong> @sent_last', [
              '@sent_last' => $result['SentLast24Hours'],
            ]) . '<br />',
          ],
          'send_rate' => [
            '#markup' => $this->t('<strong>Maximum Send Rate:</strong> @send_rate
            emails/second', ['@send_rate' => $result['MaxSendRate']]),
          ],
        ],
      ];
    }

    return $statistics;
  }

}
