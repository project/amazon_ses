<?php

namespace Drupal\amazon_ses;

use Aws\Exception\CredentialsException;
use Aws\Result;
use Aws\SesV2\Exception\SesV2Exception;
use Drupal\amazon_ses\Event\MailSentEvent;
use Drupal\aws\AwsClientFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Mime\Email;

/**
 * Amazon SES service.
 */
class AmazonSesHandler implements AmazonSesHandlerInterface {
  use StringTranslationTrait;

  /**
   * The AWS SesClient.
   *
   * @var \Aws\SesV2\SesV2Client
   */
  protected $client;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The account quota.
   *
   * @var array
   */
  protected $quota = [];

  /**
   * Constructs the service.
   *
   * @param \Drupal\aws\AwsClientFactoryInterface $aws_client_factory
   *   The AWS client factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(AwsClientFactoryInterface $aws_client_factory, LoggerChannelInterface $logger, MessengerInterface $messenger, EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $config_factory) {
    $this->client = $aws_client_factory->getClient('sesv2');
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->eventDispatcher = $event_dispatcher;
    $this->config = $config_factory->get('amazon_ses.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function send(Email $email) {
    try {
      $result = $this->client->sendEmail([
        'Content' => [
          'Raw' => [
            'Data' => $email->toString(),
          ],
        ],
      ]);

      $event = new MailSentEvent($result['MessageId'], $email);
      $this->eventDispatcher->dispatch($event, MailSentEvent::SENT);

      $throttle = $this->config->get('throttle');
      if ($throttle) {
        $sleep_time = $this->getSleepTime();
        usleep($sleep_time);
      }

      return $result['MessageId'];
    }
    catch (CredentialsException $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }
    catch (SesV2Exception $e) {
      $this->logger->error($e->getAwsErrorMessage());
      return FALSE;
    }
  }

  /**
   * Get the number of microseconds to pause for throttling.
   *
   * @return int
   *   The time to sleep in microseconds.
   */
  protected function getSleepTime() {
    $results = $this->getSendQuota();

    // Avoid a division by zero if the quota call failed.
    if (empty($results)) {
      return 0;
    }

    $multiplier = $this->config->get('multiplier');
    $per_second = ceil((1000000 * $multiplier) / $results['MaxSendRate']);

    return intval($per_second);
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentities() {
    $identities = [];

    try {
      $results = $this->client->listEmailIdentities();

      foreach ($results['EmailIdentities'] as $emailIdentity) {
        $identity = $emailIdentity['IdentityName'];
        $result = $this->client->getEmailIdentity([
          'EmailIdentity' => $identity,
        ]);

        $domain = $result['IdentityType'] == 'DOMAIN';
        $item = [
          'identity' => $identity,
          'status' => $result['VerifiedForSendingStatus'] ? 'Success' : 'Not verified',
          'type' => $domain ? 'Domain' : 'Email Address',
        ];

        if ($domain) {
          $item['dkim'] = $result['DkimAttributes']['SigningEnabled'] ? 'Enabled' : 'Disabled';
        }
        else {
          $item['dkim'] = '';
        }

        $identities[] = $item;
      }
    }
    catch (SesV2Exception $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError($this->t('Unable to list identities.'));
    }

    return $identities;
  }

  /**
   * {@inheritdoc}
   */
  public function verifyIdentity($identity) {
    $this->client->createEmailIdentity([
      'EmailIdentity' => $identity,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteIdentity($identity) {
    $this->client->deleteEmailIdentity([
      'EmailIdentity' => $identity,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSendQuota() {
    if (empty($this->quota)) {
      try {
        $result = $this->client->getAccount();
        $this->quota = array_map('number_format', $result['SendQuota']);
      }
      catch (SesV2Exception $e) {
        $this->logger->error($e->getMessage());
        $this->messenger->addError($this->t('Unable to retrieve quota.'));
      }
    }
    return $this->quota;
  }

  /**
   * Return the result data as an array.
   *
   * @param \Aws\Result $result
   *   The result from the API call.
   *
   * @return array
   *   The result data.
   */
  protected function resultToArray(Result $result) {
    $array = $result->toArray();
    unset($array['@metadata']);

    return $array;
  }

}
