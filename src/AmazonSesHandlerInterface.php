<?php

namespace Drupal\amazon_ses;

use Symfony\Component\Mime\Email;

/**
 * Interface for the Amazon SES handler.
 */
interface AmazonSesHandlerInterface {

  /**
   * Get the AWS SES client.
   *
   * @return \Aws\SesV2\SesV2Client
   *   The AWS SES client.
   */
  public function getClient();

  /**
   * Send an email using the AWS SDK.
   *
   * @param \Symfony\Component\Mime\Email $email
   *   An Email object representing the prepared message.
   *
   * @return string|bool
   *   The message ID tf successful, or FALSE if an error occurs.
   */
  public function send(Email $email);

  /**
   * Get verified identities.
   *
   * @return array
   *   An array of verified identities.
   */
  public function getIdentities();

  /**
   * Verify an identity.
   *
   * @param string $identity
   *   The identity to verify.
   */
  public function verifyIdentity($identity);

  /**
   * Delete a verified identity.
   *
   * @param string $identity
   *   The identity to delete.
   */
  public function deleteIdentity($identity);

  /**
   * Get sending quota.
   *
   * @return array
   *   An array of quota information.
   */
  public function getSendQuota();

}
