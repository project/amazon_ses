<?php

namespace Drupal\amazon_ses\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Amazon SES settings form.
 */
class AmazonSesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_ses_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['amazon_ses.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('amazon_ses.settings');

    $form['from_address'] = [
      '#type' => 'email',
      '#title' => $this->t('From Address'),
      '#description' => $this->t('The address emails will be sent from. This
        address must be verified by SES.'),
      '#default_value' => $config->get('from_address'),
      '#required' => TRUE,
    ];

    $form['from_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Name'),
      '#description' => $this->t('The email from name. Defaults to the site name.'),
      '#default_value' => $config->get('from_name') ?? $this->config('system.site')->get('name'),
    ];

    $form['override_from'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override From Email'),
      '#description' => $this->t('If set, all emails will be sent using the
        configured name and address. Any value set by another module will be
        overridden.'),
      '#default_value' => $config->get('override_from'),
    ];

    $form['throttle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Throttle'),
      '#description' => $this->t('Throttle the sending. Helpful to prevent
        exceeding the rate limit when send a high volume of emails.'),
      '#default_value' => $config->get('throttle'),
    ];

    $form['multiplier'] = [
      '#type' => 'number',
      '#title' => $this->t('Throttling Multiplier'),
      '#description' => $this->t('It is useful to set a multiplier when you have
        multiple PHP workers running in parallel. Setting this to the number of
        workers ensures you stay under the rate limit.'),
      '#default_value' => $config->get('multiplier') ?? 1,
      '#states' => [
        'visible' => [
          ':input[name="throttle"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Queue emails'),
      '#description' => $this->t('Emails will be placed in a queue and sent when cron runs.'),
      '#default_value' => $config->get('queue'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('amazon_ses.settings')
      ->set('from_address', $form_state->getValue('from_address'))
      ->set('from_name', $form_state->getValue('from_name'))
      ->set('override_from', $form_state->getValue('override_from'))
      ->set('throttle', $form_state->getValue('throttle'))
      ->set('multiplier', (int) $form_state->getValue('multiplier'))
      ->set('queue', $form_state->getValue('queue'))
      ->save();

    $this->messenger()->addMessage($this->t('The settings have been saved.'));
  }

}
